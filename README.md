# AngularJS frontend hospital

## Installation

Make sure to have npm installed and then run: `npm install`.
Or you can use yarn.

## Build & development

Run `npm run gulp serve` for preview.

## Testing

Running `npm run gulp test` will run the unit tests with karma.

Running `npm run gulp test:watch` will run the unit tests with karma everytime the code source changes.
