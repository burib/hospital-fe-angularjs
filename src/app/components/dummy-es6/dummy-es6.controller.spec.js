let DummyEs6Controller;

describe('Controller: DummyEs6Controller', () => {
  beforeEach(() => {
    angular.mock.inject(($controller, $rootScope) => {
      DummyEs6Controller = $controller('DummyEs6Controller as vm', { $scope: $rootScope.$new() });
    });
  });

  it('should return 0 when multiplying by 0',() => {
    expect(DummyEs6Controller.multiply(0, 2)).toEqual(0);
  });

  it('should return Infinity when dividing by 0', () => {
    expect(DummyEs6Controller.divide(3, 0)).toEqual(Number.POSITIVE_INFINITY);
  });
});
