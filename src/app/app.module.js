import angular from 'angular';

import appCore from 'app/components/core/core.module';
import appHome from 'app/components/home/home.module';
import appDummyEs6 from 'app/components/dummy-es6/dummy-es6.module';
import appDummyTs from 'app/components/dummy-ts/dummy-ts.module';

export default angular
  .module('app', [
    appCore,
    appHome,
    appDummyEs6,
    appDummyTs
  ])
  .name;
